# Need GCC 10 and binutils 2.35/master on Linux/x86-64 with glibc 2.31.

CC=gcc
CFLAGS=-O2 -g
OBJS=arparse.o arlex.o ar.o not-ranlib.o arsup.o rename.o binemul.o \
     emul_vanilla.o bucomm.o version.o filemode.o \
    libbfd-2.35-3.fc33.so libiberty.a

all: ar
	./ar rc libfoo.a not-ranlib.o

ar: $(OBJS)
	$(CC) $(CFLAGS) -o $@ -Wl,--as-needed $^ -Wl,-R,.

clean:
	rm -f ar libfoo.a st*
